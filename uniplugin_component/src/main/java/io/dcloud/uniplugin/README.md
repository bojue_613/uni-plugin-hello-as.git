利用原生android editText控件，用于在nvue页面使用

### 1，使用方法
#### html部分示例
```javascript
<editText ref="edit1" textSize="16" textColor="#1680ea" @afterTextChanged="afterTextChanged"
			hint="請掃碼" hintTextColor="#D9001B"></editText>
```
### 参数说明
|参数名	  |  说明	    |
|-------- |--------:    |
|textSize |输入框字号 |
|textColor |输入框文字颜色 |
|hint |描述文案 |
|hintTextColor |描述文案颜色 |

##### 备注
组件本身支持设置class,style,字号和颜色只能通过预定的参数设置



#### 回调方法

|方法名	  |  说明	    |
|-------- |--------:    |
|@beforeTextChanged| 内容变化前|
|@onTextChanged| 内容发送变化|
|@afterTextChanged| 内容变化后|

##### 回调返回值 ：
{detail:{text:'111'}}

#### 组件方法：

|方法名	  |  说明	    |
|-------- |--------:    |
|focus|获得焦点|
|blur|失去焦点|
|hideKeyboard|隐藏键盘|
|setText|修改值，传入需要变化的值|

##### 说明
通过定义ref别名调用组件方法
