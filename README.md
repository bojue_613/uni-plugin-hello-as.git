原生android端editText控件，用于在nvue页面使用
使用方法
<editText ref="edit1" textSize="16" textColor="#1680ea" @afterTextChanged="afterTextChanged"
hint="請掃碼" hintTextColor="#D9001B"></editText>

支持的参数有textSize（输入框字号），textColor（输入框文字颜色）hint（描述文案）	hintTextColor（描述文案颜色）

组件本身支持设置class,style,字号和颜色只能通过预定的参数设置
回调方法：
@beforeTextChanged 内容变化前
@onTextChanged 内容发送变化
@afterTextChanged 内容变化后
返回值 {detail:{text:'111'}}
方法：
通过ref调用组件方法
focus（获得焦点）
blur（失去焦点）
showKeyboard（显示键盘）
hideKeyboard（隐藏键盘）
setText（修改值）

```java
package io.dcloud.uniplugin;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import io.dcloud.feature.uniapp.UniSDKInstance;
import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.ui.action.AbsComponentData;
import io.dcloud.feature.uniapp.ui.component.AbsVContainer;
import io.dcloud.feature.uniapp.ui.component.UniComponent;
import io.dcloud.feature.uniapp.ui.component.UniComponentProp;

public class T11EditText extends UniComponent<EditText> {

    public T11EditText(UniSDKInstance instance, AbsVContainer parent, AbsComponentData basicComponentData) {
        super(instance, parent, basicComponentData);
    }

    @Override
    protected EditText initComponentHostView(Context context) {
        EditText editText = new EditText(context);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                cellJsEvent("beforeTextChanged", charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                cellJsEvent("onTextChanged", charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                cellJsEvent("afterTextChanged", editable.toString());
            }
        });
        return editText;
    }

    private void cellJsEvent(String name,String text){
        Map<String, Object> params = new HashMap<>();
        Map<String, Object> number = new HashMap<>();
        number.put("text", text);
        //目前uni限制 参数需要放入到"detail"中 否则会被清理
        params.put("detail", number);
        fireEvent(name, params);
    }

    @UniComponentProp(name = "textSize")
    public void setSize(String size){
        if(size != null && !size.equals("")){
            getHostView().setTextSize(Integer.valueOf(size));
        }

    }
    @UniComponentProp(name = "textColor")
    public void setTextColor(String color){
        if(color != null && !color.equals("")){
            getHostView().setTextColor(Color.parseColor(color));
        }

    }
    @UniComponentProp(name = "hint")
    public void setHint(String hint){
        getHostView().setHint(hint);
    }
    @UniComponentProp(name = "hintTextColor")
    public void setHintTextColor(String color){
        getHostView().setHintTextColor(Color.parseColor(color));
    }


    @UniJSMethod
    public void setText(String text){
        getHostView().setText(text);
        getHostView().setSelection(getHostView().getText().length());
    }

    @UniJSMethod
    public void focus(){
        getHostView().requestFocus();
        getHostView().setSelection(getHostView().getText().length());
    }

    @UniJSMethod
    public void blur(){
        getHostView().clearFocus();
        hideKeyboard();
    }

    @UniJSMethod
    public void showKeyboard(){
        InputMethodManager imm = (InputMethodManager) mUniSDKInstance.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(getHostView(), 0);
    }

    @UniJSMethod
    public void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) mUniSDKInstance.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getHostView().getWindowToken(), 0);
    }



}
```